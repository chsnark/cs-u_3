﻿using System;

namespace ExtraDrill_AM
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("введите три целых числа");
            // int.Parse - преобразование строки в число, в данном случае строки от ReadLine
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());

            //double result = (a + b + c) / 3;
            /// !!! если при делении тройку записать as is, то результат будет округлен автоматически. Нужно 3.0 !!!

            double result = (a + b + c) / 3.0;

            Console.WriteLine("Средн. арифметическое {0} {1} {2} равно {3}", a, b, c, Math.Round(result,2));
            // !!! оказывается, если в Math.Round передать ОДИН параметр, он по умолчанию округлится до целого !!!

            // Console.WriteLine("Средн. арифметическое {0} {1} {2} равно {3}", a, b, c, result);
            // !!! если передать result без округления, он автоматически округлится до целого !!!


            Console.ReadKey();
        }
    }
}
