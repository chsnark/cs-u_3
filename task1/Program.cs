﻿using System;
//ввод-вывод
namespace task1
{
    internal class Program
    {
        static void Main()
        {
            // вывод информации на экран
            Console.WriteLine("Привет, мир!!!!!!!!!!!!!!!!!!!!");
            int a = 1, b = 2;
            Console.WriteLine(a);
            Console.WriteLine(a + b);
            Console.WriteLine("Доход {0} Расход {1}",a,b);
            Console.WriteLine($"Вывод с обращением к имени переменной в теле текста{a}(без меток)");
            // ввод информации с клавиатуры
            // для ввода информации с клавиатуры нужна переменная, в которой хранятся данные ввода
            string name = Console.ReadLine();
            
            // тут используется пример с обращением к имени переменной в теле текста
            Console.WriteLine($"Привет, {name}");
            Console.WriteLine("Привет, {0}!", name); //альтернативное использование

            // задержка экрана
            Console.ReadLine();
        }
    }
}
