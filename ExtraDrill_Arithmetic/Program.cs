﻿using System;

namespace ExtraDrill_Arithmetic
{
    internal class Program
    {
        static void Main()
        {
            /*
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            
            // Если аргументы целочисленные, то / выполняет ЦЕЛОЧИСЛЕННОЕ деление
            Console.WriteLine(a / b);
            // Остаток от деления(целочисленного)
            Console.WriteLine(a % b);
            
            // Парс может выдавать число с заданной точностью.

            double aD = double.Parse(Console.ReadLine());
            double bD = double.Parse(Console.ReadLine());

            // Если аргументы даблы, то / выполняет ТОЧНОЕ деление
            Console.WriteLine(aD / bD);    
            Console.WriteLine(aD % bD);
            */

            const double pi = 3.1415926;
            
            Console.WriteLine("Вычисление площади круга радиусом r");
            Console.WriteLine("Введите r");
            
            int r = int.Parse(Console.ReadLine());

            double S_kruga = 2 * pi * r * r;
            Console.WriteLine("Площадь круга радиусом {0} равна {1}:", r, Math.Round(S_kruga, 3));
            Console.WriteLine("\n");
            //
            //
            Console.WriteLine("Вычисление объема цилиндра радиусом R и высоты h");
            Console.WriteLine("Введите R");
            Console.WriteLine("Введите h");

            int R = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите h");
            int h = int.Parse(Console.ReadLine());

            double V_cyl = (2 * pi * R * R) * h;
            Console.WriteLine("Объем цилиндра радиусом {0} и высоты {1} равна {2}:", R, h, Math.Round(V_cyl, 3));
            Console.WriteLine("\n");
            //        
            //
            Console.WriteLine("Вычисление площади поверхности цилиндра радиусом R и высоты h");
            Console.WriteLine("Введите R");
            R = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите h");
            h = int.Parse(Console.ReadLine());

            double S_cyl = (2 * pi * R * R) + (2 * pi * R * h);
            Console.WriteLine("Площадь поверхности цилиндра радиусом {0} и высоты {1} равна {2}:", R, h, Math.Round(S_cyl, 3));
                        
            Console.ReadKey();
        }
    }
}
