﻿using System;

namespace Comparsion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            byte value1 = 0, value2 = 1;
            bool result = false;

            // Меньше
            result = value1 < value2;
            Console.WriteLine(result);

            // Больше
            result = value1 > value2;
            Console.WriteLine(result);

            // Меньше либо равно
            result = value1 <= value2;
            Console.WriteLine(result);

            // Больше либо равно
            result = value1 >= value2;
            Console.WriteLine(result);

            // Равно
            result = value1 == value2;
            Console.WriteLine(result);

            // Не равно
            result = value1 != value2;
            Console.WriteLine(result);

            // Задержка
            Console.ReadKey();
        }
    }
}
