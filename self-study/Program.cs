﻿using System;

namespace Arithmetic_Operators
{
    internal class Program
    {
        static void Main()
        {
            // Сумма (+) 
            byte summand1 = 1, summand2 = 2; // Множественное объявление.
            int sum = 0;
            sum = summand1 + summand2;

            Console.WriteLine(sum);

            // Разность (-)
            byte minuend = 5, subtrahend = 3;
            int difference = 0;
            difference = minuend - subtrahend;

            Console.WriteLine(difference);

            // Произведение (*)
            byte factor1 = 2, factor2 = 3;
            int product = 0;
            product = factor1 * factor2;

            Console.WriteLine(product);

            // Деление (/)
            byte dividend = 5, divisor = 2;
            int quotient = 0, remainder = 0;// remainder -  остаток от деления
            quotient = dividend / divisor;

            Console.WriteLine(quotient);

            // Остаток от деления (%)
            remainder = dividend % divisor;

            Console.WriteLine(remainder);

            // Задержка
            Console.ReadKey();

        }
    }
}
