﻿using System;

namespace DZ_2_5_BigFirst
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Большее из двух чисел выводится первым, меньшее вторым\n");
            Console.Write("Введите число А: ");
            int a = int.Parse(Console.ReadLine());
            
            Console.Write("Введите число Б: ");
            int b = int.Parse(Console.ReadLine());

            if (a > b)
            {
                Console.WriteLine(a);
                Console.WriteLine(b);
            }          
            else // это когда a меньше или равно b, в обоих случаях вывод в первую очередь b удовлетворяет условиям задачи
            {
                Console.WriteLine(b);
                Console.WriteLine(a);
            }

            Console.ReadKey();

        }
    }
}
