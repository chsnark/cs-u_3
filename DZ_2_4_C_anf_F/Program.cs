﻿using System;

namespace DZ_2_4_C_anf_F
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Перевод шкалы Цельсия в шкалу Фаренгейта\n");
            Console.Write("Введите температуру в градусах Цельсия: ");
            //TC = (TF − 32)·5/9.
            //TF = (32*5/9 + TC)*9/5

            int C = Convert.ToInt32(Console.ReadLine());
            double F = (C + 32 * 5.00 / 9.00) * 1.8;
            Console.WriteLine("Температура в градусах Фаренгейта равна: {0}", Math.Round(F,2));
            
            Console.ReadKey();
        }
    }
}
