﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2_2_comparison
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Сравнение двух чисел А и Б\n");
            Console.Write("Введите число А: ");
            int a = Convert.ToInt32(Console.ReadLine());

            Console.Write("Введите число Б: ");
            int b = Convert.ToInt32(Console.ReadLine());

            if (a < b)
                Console.WriteLine("А меньше Б");
            
            else if (a > b)
                Console.WriteLine("А больше Б");
            
            else
                Console.WriteLine("А равно Б");
            
            Console.ReadKey();

        }
    }
}
