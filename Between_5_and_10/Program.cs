﻿using System;

namespace Between_5_and_10
{
    internal class Program
    {
        static void Main()
        {
            Console.WriteLine("Попадание в диапазон между 5 и 10\n");
            Console.Write("Введите число: ");

            int A = int.Parse(Console.ReadLine());

            if (A > 5 && A < 10)
                Console.WriteLine("Число больше 5 и меньше 10");
            else
                Console.WriteLine("Неизвестное число");

            Console.ReadKey();
        }
    }
}
