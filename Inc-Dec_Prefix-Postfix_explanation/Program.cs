﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inc_Dec_Prefix_Postfix_explanation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //объяснения препода-программера, прямая речь в кавычках. ПОНЯЛ - КЛЮЧЕВОЙ ЗНАК - ;
            int x = 0;
            
           //1
            Console.WriteLine(++x); // префикс означает, что инкремент производится "здесь и сейчас",
            Console.ReadKey();      // ВО ВРЕМЯ операции обозначенной точкой с запятой


            //2
            x = 0;
            Console.WriteLine(x++); // постфикс означает, что инкремент произойдет ПОСЛЕ отработки точки с запятой
            Console.WriteLine(x);
            Console.ReadKey();

            //3
            x = 0;
            x++;                    //инкремент произошел здесь и новое значени Х перешло в следующую команду
            Console.WriteLine(x++);
            Console.ReadKey();
        }
    }
}
