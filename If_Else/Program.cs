﻿using System;
using System.Xml.Linq;

namespace If_Else
{
    internal class Program
    {
        static void Main()
        {
            // Условная конструкция - if-else (с несколькими ветвями). Каскад условных операторов.

            //int a = int.Parse(Console.ReadLine());
            //int b = int.Parse(Console.ReadLine());

            string login = "admin";
            string password = "password";

            Console.Write("введите логин: ");
            string U_login = Console.ReadLine();                                         
            string U_password = "";

            if (U_login == login)
            {
                Console.Write("введите пароль: ");
                U_password = Console.ReadLine();
                    if (U_password == password)
                        Console.WriteLine("Добро пожаловать, {0}",login);
                    else
                        Console.WriteLine("пароль неверный");
            }
            else 
            {
                Console.WriteLine("неверный логин");       // Ветвь 2
            }


            // Delay.
            Console.ReadKey();

        }
    }
}
