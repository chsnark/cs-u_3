﻿using System;

namespace DZ_slovar
{
    internal class Program
    {
        static void Main()
        {

            Console.WriteLine("русско-английский ПОГОДНЫЙ переводчик на 10 слов");
            Console.WriteLine("жарко - тепло - прохладно - холодно - морозно - ветрено - душно - дождливо - сыро - сухо\n");
            Console.Write("Введите слово из предложенных: ");
            
            string user_string = Console.ReadLine();

            switch (user_string)
            {
                case "жарко":
                    Console.WriteLine("hot");
                    break;
                case "тепло":
                    Console.WriteLine("warm");
                    break;
                case "прохладно":
                    Console.WriteLine("cool");
                    break;
                case "холодно":
                    Console.WriteLine("cold");
                    break;
                case "морозно":
                    Console.WriteLine("frosty");
                    break;
                case "ветрено":
                    Console.WriteLine("windy");
                    break;
                case "душно":
                    Console.WriteLine("stuffy");
                    break;
                case "дождливо":
                    Console.WriteLine("rainy");
                    break;
                case "сыро":
                    Console.WriteLine("damp");
                    break;
                case "сухо":
                    Console.WriteLine("dry");
                    break;
                default:
                    Console.WriteLine("В словаре нет этого слова.");
                    break;
            }

            Console.ReadKey();
        }
    }
}
